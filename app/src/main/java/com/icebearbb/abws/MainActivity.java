package com.icebearbb.abws;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import io.crossbar.autobahn.websocket.WebSocketConnection;
import io.crossbar.autobahn.websocket.WebSocketConnectionHandler;
import io.crossbar.autobahn.websocket.exceptions.WebSocketException;
import io.crossbar.autobahn.websocket.types.ConnectionResponse;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebSocketConnection connection = new WebSocketConnection();
        try {
            connection.connect("ws://", new WebSocketConnectionHandler() {
                @Override
                public void onConnect(ConnectionResponse response) {
                    System.out.println("Connected to server");
                }

                @Override
                public void onOpen() {
                    //connection.sendMessage("Echo with Autobahn");
                }

                @Override
                public void onClose(int code, String reason) {
                    TextView tv=findViewById(R.id.mText);
                    tv.append("\nEOL " +reason+'\n');
                }

                @Override
                public void onMessage(String payload) {
                    TextView tv=findViewById(R.id.mText);
                    tv.append(payload+'\n');
                    System.out.println(payload);
                }
            });
        } catch (WebSocketException e) {
            e.printStackTrace();
        }

    }
}